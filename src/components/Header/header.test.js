import React from 'react';
import Header from './header';
import { mount } from 'enzyme';
import CustomProvider from '../../testSetup/redux-test';

describe('Render the Header component', () => {
  let wrapper;
  beforeEach(
    () =>
      (wrapper = mount(
        <CustomProvider>
          <Header />
        </CustomProvider>,
      )),
  );
  it('should contain Header elements', () => {
    expect(wrapper.find('.header-text').text()).toContain(' Happy Car');
    expect(wrapper.find('img').length).toEqual(1);
  });
});
