import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setSidebar } from '../../redux/actions/actions';
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';
import './header.scss';

interface RootState {
  sidebar: string;
}

const Header: React.FC = () => {
  // # todo happyCar Src URL
  const dispatch = useDispatch();
  let sidebarValue = useSelector<RootState>((state) => state.sidebar);

  return (
    <div className="header">
      <img
        className="header-logo"
        src="https://brand.happycar.de/dist/logo/logo-domains/color/com.png"
        alt="Logo"
      />
      <div className="header-text">
        {' '}
        Happy Car
        <span className="float-right">
          {sidebarValue === 'block' ? (
            <CloseIcon
              className="header-icon"
              onClick={() => {
                dispatch(
                  setSidebar(sidebarValue === 'block' ? 'none' : 'block'),
                );
              }}
            />
          ) : (
            <MenuIcon
              className="header-icon"
              onClick={() => {
                dispatch(
                  setSidebar(sidebarValue === 'block' ? 'none' : 'block'),
                );
              }}
            />
          )}
        </span>
      </div>
    </div>
  );
};

export default Header;
