import React from 'react';
import { mount } from 'enzyme';
import TopOption from './topOption';
import CustomProvider from '../../testSetup/redux-test';
import './topOption.scss';

describe('Render the TopOption component', () => {
  let wrapper;
  beforeEach(
    () =>
      (wrapper = mount(
        <CustomProvider>
          <TopOption />
        </CustomProvider>,
      )),
  );

  it('should contain TopOption elements', () => {
    expect(wrapper.find('main').length).toEqual(1);
    expect(wrapper.find('ul').length).toEqual(1);
  });
});
