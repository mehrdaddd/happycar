interface IVehicle {
  category: string;
  name: string;
  imageUrl: string;
  acrissCode: string;
  gearType: string;
  bags: object | any;
  seats: number;
  doors: number;
  hasAC: boolean;
}

interface IPrice {
  currencyCode: string;
  value: number;
  taxPercentage: number | null;
  payAt: string;
}

interface IRates {
  average: number;
  count: number;
}

interface ISupplier {
  name: string;
  rawName: string;
  logoUrl: string;
  rating: IRates | null;
}

export interface ICar {
  bookingFormUrl: string;
  rateType: string;
  fuelPolicy: string;
  vehicle: IVehicle;
  supplier: ISupplier;
  hasSanitationGuarantee: boolean;
  price: IPrice;
}

export interface IFilter {
  seats: number;
  type: string;
  category: string;
  gear: string;
}
