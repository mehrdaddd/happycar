import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import Box from '@material-ui/core/Box';
import Rating from '@material-ui/lab/Rating';
import Car from './car';
import './topOption.scss';
import { ICar, IFilter } from './carInterface';
import mockData from '../../assets/mockdata.json';

const TopOption: React.FC = () => {
  const { t } = useTranslation();
  const [data, setData] = useState<ICar[]>(mockData);
  let state: IFilter = useSelector((store: IFilter) => store);

  useEffect(() => {
    const filterData = mockData.filter(
      (applyTag) =>
        (state.type !== 'all' ? applyTag.rateType === state.type : true) &&
        (state.seats !== 0 ? applyTag.vehicle.seats === state.seats : true) &&
        (state.category !== 'all'
          ? applyTag.vehicle.category === state.category
          : true) &&
        (state.gear !== 'all'
          ? applyTag.vehicle.gearType === state.gear
          : true),
    );
    setData(filterData);
  }, [state]);

  return (
    <main className="option-page">
      <ul className="option-page--posts margin-top-3">
        {data.map((panel: ICar, i: number) => {
          return (
            <li key={i} className="option-with">
              <section className="option-rolPadding">
                <header className="option-header">
                  {panel.supplier.rating && (
                    <React.Fragment>
                      <Box
                        component="fieldset"
                        mb={1}
                        borderColor="transparent"
                      >
                        <Rating
                          name="read-only"
                          max={10}
                          defaultValue={panel.supplier.rating.average * 10}
                          readOnly
                        />
                      </Box>
                      <Box className="font-size-small">
                        {t('No. Rates')}: {panel.supplier.rating.count}{' '}
                      </Box>
                    </React.Fragment>
                  )}
                </header>
                <Car panel={panel} />
              </section>
            </li>
          );
        })}
      </ul>
    </main>
  );
};

export default TopOption;
