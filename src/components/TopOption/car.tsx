import React from 'react';
import { useTranslation } from 'react-i18next';
import Box from '@material-ui/core/Box';
import { ICar } from './carInterface';
import SeatIcon from '@material-ui/icons/AirlineSeatReclineExtra';
import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight';
import './topOption.scss';

// #todo happyCar use icons instead of text
const Car: React.FC<{ panel: ICar }> = (props) => {
  const { t } = useTranslation();

  return (
    <article className="option-rol">
      <a href={props.panel.bookingFormUrl}>
        <div>
          <div className="option-container">
            <img
              src={props.panel.vehicle.imageUrl}
              alt="car image"
              style={{ height: '9em' }}
            />
            <header className="margin-left-3">
              <p className="option-part-Header">
                {' '}
                {t('category')}: {props.panel.vehicle.category}
              </p>
              <p className="option-part-Header">
                {' '}
                {t('name')}: {props.panel.vehicle.name}
              </p>
              <p className="option-part-Header">
                <SeatIcon />: {props.panel.vehicle.seats}
              </p>
              <p className="option-part-Header">
                {' '}
                {t('doors')}: {props.panel.vehicle.doors}
              </p>
              <p className="option-part-Header">
                {' '}
                {t('bags')}: {props.panel.vehicle.bags.min}
              </p>
            </header>
          </div>
          <Box className="option-head-text margin">
            {t('Price')}: {props.panel.price.value.toFixed(2)}
            {props.panel.price.currencyCode}
          </Box>
        </div>
        <div className="option-footer">
          <p className="option-part-Header margin08">
            {' '}
            {t('supplier name')}: {props.panel.supplier.name}
          </p>
          <img
            src={props.panel.supplier.logoUrl}
            alt="car image"
            style={{ height: '2em' }}
          />
          <div className="hover-background border">
            {t('Booking')}
            <span>
              <SubdirectoryArrowRightIcon />
            </span>
          </div>
        </div>
      </a>
    </article>
  );
};

export default Car;
