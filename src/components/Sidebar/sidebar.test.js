import React from 'react';
import {mount} from 'enzyme';
import Sidebar from './sidebar';
import CustomProvider from '../../testSetup/redux-test';
import {fireEvent, render} from '@testing-library/react';

describe('Render the sidebar component', () => {
    let wrapper;
    beforeEach(
        () =>
            (wrapper = mount(
                <CustomProvider>
                    <Sidebar/>
                </CustomProvider>,
            )),
    );

    it('should contain sidebar elements', () => {
        expect(wrapper.find("button").length).toEqual(1);
        expect(wrapper.find('button').text()).toContain('Reset Filters');
        expect(wrapper.find('select').length).toEqual(4);
        expect(wrapper.find('span').length).toEqual(4);
    });

    it('should selects works correctly', () => {
        const {getByLabelText} = render(
            <CustomProvider>
                {' '}
                <Sidebar/>{' '}
            </CustomProvider>,
        );
        const seats = getByLabelText('seats');
        const type = getByLabelText('type');
        const category = getByLabelText('category');
        const gear = getByLabelText('gear');

        fireEvent.change(seats, {target: {value: 4}});
        expect(seats.value).toBe('4');

        fireEvent.change(type, {target: {value: 'BASIC'}});
        expect(type.value).toBe('BASIC');

        fireEvent.change(category, {target: {value: 'MINI'}});
        expect(category.value).toBe('MINI');

        fireEvent.change(category, {target: {value: 'MINI'}});
        expect(category.value).toBe('MINI');

        fireEvent.change(gear, {target: {value: 'manual'}});
        expect(gear.value).toBe('manual');
    });

    it('should the reset button works correctly', () => {
        // ## todo happycar do the test for click and events
        // const mockCallBack = jest.fn();
        // wrapper.find('button').simulate('click');
        // expect(mockCallBack.mock.calls.length).toEqual(0);
    });

    it('should the state and redux actions works correctly', () => {
        // ## todo happycar do the test for actions
    });
});
