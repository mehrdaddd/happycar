import React, { ChangeEvent } from 'react';
import './sidebar.scss';
import { useDispatch, useSelector } from 'react-redux';
import * as action from '../../redux/actions/actions';
import { useTranslation } from 'react-i18next';
import { Box } from '@material-ui/core';

interface IFilter {
  seats: number;
  type: string;
  category: string;
  gear: string;
}

const Sidebar: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  let state: IFilter = useSelector((store: IFilter) => store);

  return (
    <div className="sidebar">
      <div className="sidebar--select margin-top-3">
        <span className="sidebar-filter-name"> {t('Offer type')}: </span>
        <select
            aria-label="type"
          value={state.type}
          onChange={(event: ChangeEvent<HTMLSelectElement>) =>
            dispatch(action.setType(event.target.value))
          }
        >
          <option value="all"> {t('All')} </option>
          <option value="BASIC"> {t('basic')} </option>
          <option value="EXCELLENT"> {t('excellent')} </option>
        </select>
      </div>

      <Box className="sidebar--select margin-top-3">
        <span  className="sidebar-filter-name"> {t('No. seats')}: </span>
        <select
            aria-label="seats"
          value={state.seats}
          onChange={(event: ChangeEvent<HTMLSelectElement>) =>
            dispatch(action.setSeats(parseInt(event.target.value)))
          }
        >
          <option value="0"> {t('All')} </option>
          <option value="1"> {t('1')} </option>
          <option value="2"> {t('2')} </option>
          <option value="3"> {t('3')} </option>
          <option value="4"> {t('4')} </option>
          <option value="5"> {t('5')} </option>
          <option value="6"> {t('6')} </option>
        </select>
      </Box>

      <Box className="sidebar--select margin-top-3">
        <span className="sidebar-filter-name"> {t('Category')}: </span>
        <select
            aria-label="category"

            value={state.category}
          onChange={(event: ChangeEvent<HTMLSelectElement>) =>
            dispatch(action.setCategory(event.target.value))
          }
        >
          <option value="all"> {t('All')} </option>
          <option value="MINI"> {t('mini')} </option>
          <option value="ECONOMY"> {t('economy')} </option>
          <option value="STATION_WAGON"> {t('st. wagon')} </option>
        </select>
      </Box>

      <Box className="sidebar--select margin-top-3">
        <span className="sidebar-filter-name"> {t('Gear Type')}: </span>
        <select
            aria-label="gear"
          value={state.gear}
          onChange={(event: ChangeEvent<HTMLSelectElement>) =>
            dispatch(action.setGear(event.target.value))
          }
        >
          <option value="all"> {t('All')} </option>
          <option value="manual"> {t('manual')} </option>
        </select>
      </Box>

      <Box className="sidebar-filter-button">
        <button
          className="button-reset"
          onClick={() => {
            dispatch(action.resetFilter());
          }}
        >
          {t('Reset Filters')}
        </button>
      </Box>
    </div>
  );
};

export default Sidebar;
