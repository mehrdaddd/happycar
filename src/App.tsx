import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { Box, Grid, Paper } from '@material-ui/core';
import TopOption from './components/TopOption/topOption';
import Sidebar from './components/Sidebar/sidebar';
import './App.scss';

interface RootState {
  sidebar: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      flexWrap: 'nowrap',
    },
    paper: {
      padding: theme.spacing(0),
      textAlign: 'center',
      color: theme.palette.text.secondary,
      marginLeft: 'auto',
      marginRight: 'auto',
      backgroundColor: '#f4f4f4',
    },
  }),
);

const App: React.FC = () => {
  let sidebarValue = useSelector<RootState>((state) => state.sidebar);
  const classes = useStyles();
  return (
    <div className="app">
      <Grid container className={classes.root} spacing={1}>
        <Box
          className="app-sidebar margin-top-3"
          style={{
            width: sidebarValue === 'none' ? '250px' : '100%',
            display: sidebarValue === 'block' ? 'block' : '',
          }}
        >
          <Sidebar />
        </Box>
        {sidebarValue === 'none' && (
          <Grid item sm={9} className={classes.paper}>
            <Paper className={classes.paper}>
              <TopOption />
            </Paper>
          </Grid>
        )}
      </Grid>
    </div>
  );
};

export default App;
