import { createStore } from 'redux';
import reducer from '../reducers/reducers';
export const initial = {
  seats: 0,
  type: 'all',
  category: 'all',
  gear: 'all',
  sidebar: 'none',
};

export default createStore(
  reducer,
  initial,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);
