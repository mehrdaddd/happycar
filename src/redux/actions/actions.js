export function setGear(value) {
  return {
    type: 'GEAR',
    value,
  };
}

export function setSidebar(value) {
  return {
    type: 'SIDEBAR',
    value,
  };
}

export function setCategory(value) {
  return {
    type: 'CATEGORY',
    value,
  };
}

export function setSeats(value) {
  return {
    type: 'SEATS',
    value,
  };
}

export function setType(value) {
  return {
    type: 'TYPE',
    value,
  };
}

export function resetFilter() {
  return {
    type: 'RESET_FILTER',
  };
}
