import { initial } from '../store/store';

export default function reducer(state, action) {
  switch (action.type) {
    case 'TYPE':
      return Object.assign({}, state, { type: action.value });

    case 'SEATS':
      return Object.assign({}, state, { seats: action.value });

    case 'CATEGORY':
      return Object.assign({}, state, { category: action.value });

    case 'GEAR':
      return Object.assign({}, state, { gear: action.value });

    case 'SIDEBAR':
      return Object.assign({}, state, { sidebar: action.value });

    case 'RESET_FILTER':
      return Object.assign({}, initial);

    default:
      return state;
  }
}
