import React from 'react';
import {mount} from 'enzyme';
import App from './App';
import CustomProvider from "./testSetup/redux-test"

describe('App', () => {
    let wrapper;
    beforeEach(() => (
        wrapper = mount(
            <CustomProvider>
                <App />
            </CustomProvider>
        )))

    it('should render a App component', () => {
        expect(wrapper.find(".app").length).toEqual(1);
        expect(true).toBe(true);
     })
})


